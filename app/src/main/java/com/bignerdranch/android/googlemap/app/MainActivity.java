package com.bignerdranch.android.googlemap.app;


import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;

public class MainActivity extends Activity {

    private GoogleMap mGoogleMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //the following get the current latitude and longitude
        Location currentLocation = mGoogleMap.getMyLocation();
        double latitudeCurr = currentLocation.getLatitude();
        double longitudeCurr = currentLocation.getLongitude();

        //the following will do the navigation
        //We need to write the destination latitude and logitude to make it work
        final Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://maps.google.com/maps?"+"saddr="
                        + latitudeCurr + "," + longitudeCurr + "&daddr="
                        + latitude + "," + longitude));
        intent.setClassName("com.google.android.apps.maps",
                "com.google.android.maps.MapsActivity");
        startActivity(intent);
    }
}